import os
import json
from jiexi_json.preliminary_analysis import PreAnalysis
from jiexi_json.analysis import Analy


class RegainJson(object):
    def __init__(self, json_file=None):
        if json_file:
            assert os.path.isfile(json_file),\
                "error:Env json file(% s) is error!!!" % json_file
            self.json_file = json_file
        else:
            self.json_file = os.path.join(os.path.dirname(__file__), 'test.json')
        self.data = self._load_json()
        self.result_dict, self.type_list, self.type = self._get_result()

    # 解析json文件转化为python字典
    def _load_json(self):

        """
        :return:解析json数据-->Python字典并返回
        """
        with open("base.json", 'r') as f:
            data = json.load(f)
        return data

    # 得到原json文件中需要的数据
    def _get_result(self):
        an = PreAnalysis(self.json_file)
        result_dict = an.result_dict
        type_list = an.list_type
        type = an.type
        return result_dict, type_list, type

    def test(self):
        pass

    # 写入到新的json文件中
    def write(self):

        for type_list in self.type_list:
            if type_list[0] == 'DCU':
                type_list[0] = 'dcu'
            else:
                type_list[0] = 'meu'
            if type_list[2] == 'ENG':
                type_list[2] = 'type_eng'
            else:
                type_list[2] = 'type_user'

        for key in self.result_dict.keys():
            for li in self.type_list:
                if key == li[1]:
                    # print(li)
                    if 'T0' in li[1]:
                        li[1] = 'T0'

                    elif 'T1' in li[1]:
                        li[1] = 'T1'

                    elif 'T2' in li[1]:
                        li[1] = 'T2'

                    elif 'L1DOT5' in li[1]:
                        li[1] = 'L1.5'

                    elif 'L1' in li[1]:
                        li[1] = 'L1'

                    elif 'L2' in self.type or 'NR' in self.type:
                        li[1] = 'L2'

                    elif 'TEMV' in self.type:
                        li[1] = 'TEMV'

                    for toland in self.result_dict[key]:
                        self.data[li[1]][li[0]][toland][li[2]] = 'true'

        a = Analy(self.json_file)
        branch = a.get_branch()
        new_branch = branch.replace(r'/', '_').replace(r'-', "_")
        file_abs = new_branch+'.json'

        with open(file_abs, 'w') as f:
            f.write(json.dumps(self.data, indent=4, ensure_ascii=False))


if __name__ == "__main__":
    a = RegainJson("test1.json")
    a.write()









