import json
from jiexi_json.preliminary_analysis import PreAnalysis


class TreeGridJson(object):
    def __init__(self, json_file):
        self.json_file = json_file
        self.result_dict, self.type_list, self.type = self._read()
        self.data = self.load_json()
    # 构造一个preanalysis对象,获取解析到的有效数据

    def _read(self):
        """

        :return:
            result_dict:有效仕向地信息,Python字典
                {"机型":"仕向地","机型":"仕向地"}

            list_type: DCU MEU USER ENG 和机型三个
                元素组成的所有列表组成的列表
                如[[dcu,l1,user],[meu,l1.5,eng]] 格式

            type: manage project  如"17cy_T1T2_manager"
        """
        re = PreAnalysis(self.json_file)
        return re.result_dict, re.list_type, re.type

    # 解析json文件转化为python字典
    def load_json(self):
        """
        :return:不同分支解析不同的base json文件

        """
        if 'L1' in self.type:
            with open("L1L1.5.json", 'r') as f:
                data = json.load(f)
            return data
        elif 'T0' in self.type:
            with open("T0.json", 'r') as f:
                data = json.load(f)
            return data
        elif 'T1T2' in self.type:
            with open("T1T2.json", 'r') as f:
                data = json.load(f)
            return data
        elif 'L2' in self.type:
            with open("L2.json", 'r') as f:
                data = json.load(f)
            return data
        elif 'TEMV' in self.type:
            with open("TEMV.json", 'r') as f:
                data = json.load(f)
            return data
        elif 'NR' in self.type:
            with open("L2.json", 'r') as f:
                data = json.load(f)
                return data
        elif 'OR' in self.type:
            with open("L1L1.5.json", 'r') as f:
                data = json.load(f)
            return data

    # 处理得到的原JSON数据,将需要的数据规范到新的json数据中
    def deal_with_data(self):
        """

        :return: 根据相关信息修改json数据
        """
        for key in self.result_dict.keys():
            for li in self.type_list:
                if key == li[1]:
                    if key == li[1]:
                        # 处理一下机型关键字,转换为从base json文件中获取的关键字一致
                        if 'T0' in li[1]:
                            li[1] = 'T0'

                        elif 'T1' in li[1]:
                            li[1] = 'T1'

                        elif 'T2' in li[1]:
                            li[1] = 'T2'

                        elif 'L1DOT5' in li[1]:
                            li[1] = 'L1.5'

                        elif 'L1' in li[1]:
                            li[1] = 'L1'
                        # 原json中的L2和TEMV的release选项参数一样,无法判断,通过manage project来进行判断
                        elif 'L2' in self.type or 'NR' in self.type:
                            li[1] = 'L2'

                        elif 'TEMV' in self.type:
                            li[1] = 'TEMV'

                        # 遍历解析到的数据,查找符合条件的 将值设置为ture
                        for land in self.result_dict[key]:
                            for x in self.data:
                                if x['name'] == li[0]:
                                    for y in x['children']:
                                        if y['name'] == li[1]:
                                            for z in y["children"]:
                                                if z['name'] == land:
                                                    z[li[2].lower()] = 'true'

    # 将修改后的json数据写入json文件中
    def write(self):
        """
        :return:写操作
        """
        with open("0.json", 'w') as f:
            f.write(json.dumps(self.data, indent=4, ensure_ascii=False))


if __name__ == "__main__":
    a = TreeGridJson("test4.json")
    a.deal_with_data()
    a.write()













