import os
import json


class Analy(object):
    def __init__(self, json_file=None):
        if json_file:
            assert os.path.isfile(json_file),\
                "error:Env json file(% s) is error!!!" % json_file
            self.json_file = json_file
        else:
            self.json_file = os.path.join(os.path.dirname(__file__), 'test.json')

        self.data = self._load_json()
        self.info_dict = {}
        self.info_dict = self._get_duration()

    # 解析json文件转化为python字典
    def _load_json(self):

        """
        :return:解析json数据-->Python字典并返回
        """
        with open(self.json_file, 'r') as f:
            data = json.load(f)
        return data

    # 将需要的数据重新格式化为一个字典
    def _get_duration(self):

        """
        :return: 重新建立一个字典并返回
        """
        for data in self.data['actions'][0]['parameters']:
            self.info_dict[data['name']] = data['value']
        return self.info_dict

    # 获取num号
    def get_num(self):

        """
        :return:num号
        """
        return self.info_dict['JK_NUMBER']

    # 获取project号
    def get_project(self):

        """
        :return:project工程
        """
        return self.info_dict['JK_PROJECT']

    # 获取branch
    def get_branch(self):

        """
        :return:branch分支
        """
        return self.info_dict['JK_BRANCH']

    # 获取version
    def get_version(self):

        """
        :return:version版本
        """
        return self.info_dict['JK_VERSION']

    # 获取tag
    def get_tag(self):

        """
        :return:tag
        """
        return self.info_dict['JK_TAG']

    # 获取是否打tag(布尔值)
    def get_create_tag(self):

        """
        :return:create_tag
        """
        return self.info_dict['JK_CREATE_TAG']

    # 获取是否编译(布尔值)
    def get_create_release(self):

        """
        :return:create_release
        """
        return self.info_dict['JK_RELEASE']


if __name__ == "__main__":
    analysis = Analy()
    print(analysis.get_num())
    print(analysis.get_branch())
    print(analysis.get_project())
    print(analysis.get_version())
    print(type(analysis.get_create_release()))
    print(type(analysis.get_create_tag()))
