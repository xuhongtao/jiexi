import os
import json


class PreAnalysis(object):

    def __init__(self, json_file=None):
        if json_file:
            assert os.path.isfile(json_file),\
                "error:Env json file(% s) is error!!!" % json_file
            self.json_file = json_file
        else:
            self.json_file = os.path.join(os.path.dirname(__file__), 'test2.json')
        self.type = ""
        self.data = self._load_json()
        self.info_dict = {}
        self.info_dict = self._get_duration()
        self.list_type = []
        self.list_test = []
        self.result_dict = {}
        self.type = self.get_type()
        self.result_dict, self.list_type = self._get_grade_other()

    # 解析json文件转化为python字典
    def _load_json(self):

        """
        :return:解析json数据-->Python字典并返回
        """
        with open(self.json_file, 'r') as f:
            data = json.load(f)
        return data

    # 将需要的数据重新格式化为一个字典
    def _get_duration(self):

        """
        :return: 重新建立一个字典并返回
        """
        for data in self.data['actions'][0]['parameters']:
            self.info_dict[data['name']] = data['value']
        return self.info_dict

    def get_type(self):
        return self.data["subBuilds"][0]["parentJobName"]

    def _get_grade_other(self):
        for key in self.info_dict.keys():
            if "JK_RELEASE_" in key:
                if self.info_dict[key]:
                    # print(self.type)
                    if 'L2' in self.type or 'TEMV' in self.type:
                        self.list_type.append([key.split("_")[a] for a in range(2, 6)])
                    else:
                        self.list_type.append([key.split("_")[a] for a in range(2, 5)])
                    self.list_test.append(key.split("_")[3])
        for x in self.list_type:
            if len(x) == 4:
                x.remove("TEMV")
        for k in self.info_dict.keys():
            if "TARGET" in k:
                for i in set(self.list_test):
                    if i == k.split("_")[1]:
                        self.result_dict[k.split("_")[1]] = self.info_dict[k].split(" ")
                    elif i == k.split("_")[2]:
                        self.result_dict[k.split("_")[2]] = self.info_dict[k].split(" ")
        return self.result_dict, self.list_type


if __name__ == "__main__":
    analysis = PreAnalysis("test3.json")
    print(analysis.list_type)
    print(analysis.result_dict)






